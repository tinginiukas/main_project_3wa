<?php
/**
 * Created by PhpStorm.
 * User: student6
 * Date: 14/08/16
 * Time: 14:07
 */
class UserModel{
//*********************************** SIGN UP ***************************************

    /**
     * dsgdsgrfx
     *
     * @param $data
     * @return array
     */
    public function signUp($data){
        $newUser = $this->userExist($data['email']);
        $pswMatch = $data['password'] == $data['confirm'];
        if($newUser && $pswMatch){
            $signUpStatus = [true, 0];
            $this->create($data);
            $this->logIn($data);
        }else if(!$newUser && $pswMatch){
            $signUpStatus = [false, 1];
        }else if($newUser && !$pswMatch){
            $signUpStatus = [false, 2];
        }else{
            $signUpStatus = [false, 3];
        }
        return $signUpStatus;
    }

    private function create($data){
        $dataBase = new Database();
        $sql = 'INSERT INTO Users (`Firstname`,`Lastname`,`Email`,`Password`,`Birthdate`,`Address`,`City`,`ZipCode`,`Country`,`Phone`) VALUES (?,?,?,?,?,?,?,?,?,?)';
        $dataToInsert =[
            $data['firstname'],
            $data['lastname'],
            $data['email'],
            md5($data['password']),
            $data['birthdate'],
            $data['address'],
            $data['city'],
            $data['zipcode'],
            $data['country'],
            $data['phone']
        ];
        $result = $dataBase->executeSql($sql, $dataToInsert);
        return $result;
    }

    private function userExist($email){
        $dataBase = new Database();
        $sql = "SELECT Email FROM Users WHERE Email = ?";
        $result = $dataBase->query($sql, [$email]);
        $userExist = count($result);
        if($userExist==0){
            return true;
        }else{
            return false;
        }

    }

//**************************************************** LOG IN ************************************************
    public function logIn($data){
       $dataBase = new Database();
        $sql = "SELECT * FROM Users WHERE Email = ? AND Password = ?";
        $dataToInsert = [$data['email'], md5($data['password'])];
        $result = $dataBase->queryOne($sql, $dataToInsert);
        if($result){
            unset($_SESSION['cart']);
            $_SESSION['userInfo'] = $result;
            $_SESSION['logged'] = true;
            $this->updateLogInTime($_SESSION['userInfo']['Id']);
            $loginStatus = true;
            return $loginStatus;

        }else{
            return false;
        }
    }
    private function updateLogInTime($id){
        $dataBase = new Database();
        ini_set('date.timezone', 'Europe/Vilnius');
        $sql = "UPDATE `Users` SET `LastLoginTimestamp`= '".date('Y-m-d H:i:s')."' WHERE Id = ?";
        $dataBase->executeSql($sql, [$id]);
    }

//**************************************************** LOG OUT ************************************************
    public function logOut(){
//        unset($_SESSION['logged']);
//        unset($_SESSION['userInfo']);
        session_destroy();

    }

//***************************************************** EDIT USER *****************************************************



    private function getUserData($id){
        $dataBase = new Database();
        $sql = "SELECT * FROM Users WHERE Id=?";
        $result = $dataBase->queryOne($sql,[$id]);
        return $result;
    }

    public function updateUserData($data){
        $dataBase = new Database();
        if ($data['password'] != "" && $data['newPassword'] != "" ){
            $newPswMatch = $data['newPassword'] == $data['confirm'];
            $pswMatch = md5($data['password']) == $_SESSION['userInfo']['Password'];

            if($newPswMatch && $pswMatch){
                $sql = "UPDATE `Users` SET `Firstname`='".$data['firstname']."',`Lastname`='".$data['lastname']."',`Email`='".$data['email']."',`Password`='".md5($data['newPassword'])."',`Birthdate`='".$data['birthdate']."',`Address`='".$data['address']."',`City`='".$data['city']."',`ZipCode`='".$data['zipcode']."',`Country`='".$data['country']."',`Phone`='".$data['phone']."' WHERE `Id`='".$_SESSION['userInfo']['Id']."'";
                $dataBase->query($sql);
                $_SESSION["userInfo"] = $this->getUserData($_SESSION['userInfo']['Id']);
                return $editStatus = true;

            }else{
                return $editStatus = false;
            }

        }else{
            $sql = "UPDATE `Users` SET `Firstname`='".$data['firstname']."',`Lastname`='".$data['lastname']."',`Email`='".$data['email']."',`Birthdate`='".$data['birthdate']."',`Address`='".$data['address']."',`City`='".$data['city']."',`ZipCode`='".$data['zipcode']."',`Country`='".$data['country']."',`Phone`='".$data['phone']."' WHERE `Id`='".$_SESSION['userInfo']['Id']."'";
            $dataBase->query($sql);
            $_SESSION["userInfo"] = $this->getUserData($_SESSION['userInfo']['Id']);
            return $editStatus = true;
        }
    }

//*************************************************************************


}