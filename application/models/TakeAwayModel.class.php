<?php
/**
 * Created by PhpStorm.
 * User: student6
 * Date: 16.8.12
 * Time: 10.41
 */
class TakeAwayModel {

    public function listAll(){
        $database = new Database();
        $sql = "SELECT * FROM Meal";
        return $database->query($sql);
    }

    public function addToCart($data){
        $productKey = false;
        $found = false;
        if(isset($_SESSION['cart'])){
            foreach ($_SESSION['cart'] as $key => $cart_item){
                if($cart_item['id'] == $data['productId']){
                    $found = true;
                    if($data['type'] == 'ajax') {
                        $_SESSION['cart'][$key]['quantity'] += $data['quantity'];
                        $_SESSION['cart'][$key]['total'] += ($data['quantity']) * ($_SESSION['cart'][$key]['price']);
                        $productKey = $_SESSION['cart'][$key];
                    }elseif($data['type'] == 'update'){
                        $_SESSION['cart'][$key]['quantity'] = $data['quantity'];
                        $_SESSION['cart'][$key]['total'] = ($data['quantity']) * ($_SESSION['cart'][$key]['price']);
                        $productKey = $_SESSION['cart'][$key];
                    }elseif($data['type'] == 'remove'){
                        unset($_SESSION['cart'][$key]);
                    }
                }
            }
        }

        if(!$found) {
            $dataBase = new Database();
            $sql = "SELECT * FROM Meal WHERE `Id`=?";
            $meal = $dataBase->queryOne($sql, [$data['productId']]);

            $cartItem = [
                'id' => $meal['Id'],
                'name' => $meal['Name'],
                'photo' => $meal['Photo'],
                'quantity' => $data['quantity'],
                'price' => $meal['SalePrice'],
                'total' => $data['quantity'] * $meal['SalePrice']
            ];

            $_SESSION['cart'][] = $cartItem;
        }

        $totalItem = $this->sumAll($_SESSION['cart'],'quantity');
        $_SESSION['totalItems'] = $totalItem;

        $totalAmount = $this->sumAll($_SESSION['cart'], 'total');
        $_SESSION['totalAmount'] = $totalAmount;
        return [
            $productKey,
            $totalItem,
            $totalAmount
            ];


    }
    private function sumAll($array, $key){
        $sum = 0;
        foreach ($array as $item){
            if(isset($item[$key])){
                $sum += $item[$key];
            }
        }
        return $sum;
    }


//    ******************************************** PAY ****************************************

    public function confirmPay($data, $totalAmount, $userId = 3){
        $taxRate = 0.21;
        $taxAmount = $totalAmount*$taxRate;
        $dataBase = new Database();
        $sql = "INSERT INTO `Orders`(`User_Id`, `TotalAmount`, `TaxRate`, `TaxAmount`) VALUES (?,?,?,?)";
        $dataToInsert = [$userId, $totalAmount, $taxRate, $taxAmount];

        $pay = $dataBase->executeSql($sql, $dataToInsert);

        foreach ($data as $orderline) {
            $sql2 = "INSERT INTO `Orderline`( `QuantityOrdered`, `Meal_Id`, `Order_Id`, `PriceEach`) VALUES (?,?,?,?)";
            $dataToInsert2 = [$orderline['quantity'], $orderline['id'], $pay, $orderline['price']];
            $dataBase->executeSql($sql2, $dataToInsert2);


            $sql7 = "SELECT `QuantityInStock` FROM `Meal` WHERE `Id`=?";

            $quantityIn = $dataBase->executeSql($sql7, [$orderline['id']]);
            $newQuantity = $quantityIn - $orderline['quantity'];


            $sql3= "UPDATE `Meal` SET `QuantityInStock`=". $newQuantity ." WHERE `Id`= ".$orderline['id'];
            $dataBase->query($sql3);
        }
    }

}