'use strict';

/////////////////////////////////////////////////////////////////////////////////////////
// FONCTIONS                                                                           //
/////////////////////////////////////////////////////////////////////////////////////////

// date and time picker options
$(function(){
    $( "#datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
    $("#timepicker").timepicker({
        hours: { starts: 11, ends: 23 },
        minutes: { interval: 15 },
        rows: 3,
        showPeriodLabels: false,
        minuteText: 'Min'
    });

});

function updateCart(obj, type){


    var productId = $(obj).data('product-id');
    var quantity = $(obj).parent().parent().find('input[type=number]').val();


    $.ajax({
        method: 'POST',
        url: 'takeaway',
        data: {
            productId: productId,
            quantity: quantity,
            type:type
        },
        dataType:'json',
        success: function (response) {
            $('#totalItems').html(response.totalItems);
            $('#totalAmount').html(response.totalAmount);

            if(type == 'remove'){
                $('#response' + productId).remove();

            }else if(type == 'update'){
                $('#response' + productId).find('.total').html(response.itemAmount);
            }
        }
    })
}

$(document).ready(function () {
    $(".addToCart").click(function (e) {
        e.preventDefault();
        updateCart(this, 'ajax');
    })
    $(".updateCart").click(function (e) {
        e.preventDefault();
        updateCart(this, 'update');
    })
    $(".removeFromCart").click(function (e) {
        e.preventDefault();
        updateCart(this, 'remove');
    })

})




///////////////////////////////////GOOGLE MAP/////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////
// CODE PRINCIPAL                                                                      //
/////////////////////////////////////////////////////////////////////////////////////////


