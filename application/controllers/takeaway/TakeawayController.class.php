<?php
/**
 * Created by PhpStorm.
 * User: student6
 * Date: 16.8.11
 * Time: 12.59
 */
class TakeAwayController{
    public function httpGetMethod(Http $http, array $queryFields)
    {

        if(isset($_GET['succsses'])){
            $confirm = new TakeAwayModel();
            isset($_SESSION['logged'])?
            $pay = $confirm->confirmPay($_SESSION['cart'], $_SESSION['totalAmount'], $_SESSION['userInfo']['Id'] ):
            $pay = $confirm->confirmPay($_SESSION['cart'], $_SESSION['totalAmount']);

            unset($_SESSION['cart']);
            unset($_SESSION['totalAmount']);
            unset($_SESSION['totalItems']);
        }
        $newModel = new TakeAwayModel();
        $allMeals = $newModel->listAll();
        return['allMeals'=>$allMeals];



    }

    public function httpPostMethod(Http $http, array $formFields)
    {
        $order = new TakeAwayModel();

        if(isset($formFields['type']) && ($formFields['type'] == 'ajax' || $formFields['type'] == 'update' || $formFields['type'] == 'remove')) {

            $addMeal = $order->addToCart($formFields);

            $http->sendJsonResponse([
                'totalItems' => $addMeal[1],
                'totalAmount' => $addMeal[2],
                'itemAmount' => $addMeal[0]['total']
            ]);
        }


    }
}