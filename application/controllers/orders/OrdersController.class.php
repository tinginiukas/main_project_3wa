<?php
/**
 * Created by PhpStorm.
 * User: student6
 * Date: 16.8.11
 * Time: 10.07
 */
class OrdersController{
     public function httpGetMethod(Http $http, array $queryFields)
{
     /* Called when HTTP GET request
      *
      * Http argument $ is an object to make redirects etc.
      * The argument $ QueryFields contains the equivalent of $ _GET native PHP.
      */
}

    public function httpPostMethod(Http $http, array $formFields)
{
    /*
     *
     *      * Called when HTTP POST request
     *
     * Http argument $ is an object to make redirects etc.
     * The argument $ FormFields contains the equivalent of $ _POST native PHP.
     */
}
}