<?php
/**
 * Created by PhpStorm.
 * User: student6
 * Date: 16.8.22
 * Time: 09.43
 */
class UserController{
    public function httpGetMethod(Http $http, array $queryFields)
    {
        $action = new UserModel();
        if($queryFields['action'] == 'logout'){
            $action->logOut();
            $http->redirectTo("");
        }

    }

    public function httpPostMethod(Http $http, array $formFields)
    {
        $action = new UserModel();

        if($formFields['action'] == 'login'){
            $loginStatus = $action->logIn($formFields);
            if(!isset($_SERVER['HTTP_REFERER'])){
                !$loginStatus?$http->redirectTo("../index.php?LoginError"):$http->redirectTo("");
            }elseif((strpos($_SERVER['HTTP_REFERER'], '?LoginError') !== false) || (strpos($_SERVER['HTTP_REFERER'], '&LoginError') !== false)){
                header('Location:' . $_SERVER['HTTP_REFERER']);
            }elseif (strpos($_SERVER['HTTP_REFERER'], '?') !== false){
                !$loginStatus?header('Location:'. $_SERVER['HTTP_REFERER'].'&LoginError'):header('Location:'. $_SERVER['HTTP_REFERER']);
            }else{
                !$loginStatus?header('Location:'. $_SERVER['HTTP_REFERER'].'?LoginError'):header('Location:'. $_SERVER['HTTP_REFERER']);
            }


        }else if($formFields['action'] == 'signup'){
            $signUpStatus = $action->signUp($formFields);
            if(!$signUpStatus[0]){
                $http->redirectTo('user?action=signup&error='.$signUpStatus[1].'');
            }else{
                $http->redirectTo('../index.php?message');

            }

        }else if($formFields['action'] == 'edit'){
            $editStatus = $action->updateUserData($formFields);
            if(!$editStatus){
                $http->redirectTo('user?action=edit&failed');
            }else{
                $http->redirectTo('user?action=edit&success');
            }

        }
    }
}