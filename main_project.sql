-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 30, 2016 at 01:19 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `main_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `Booking`
--

CREATE TABLE `Booking` (
  `Id` int(11) NOT NULL,
  `BookingDate` date NOT NULL,
  `BookingTime` time NOT NULL,
  `NumberOfSeats` smallint(6) NOT NULL,
  `User_Id` int(11) NOT NULL,
  `CreationTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Meal`
--

CREATE TABLE `Meal` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Photo` varchar(30) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `QuantityInStock` tinyint(4) NOT NULL,
  `BuyPrice` double NOT NULL,
  `SalePrice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Meal`
--

INSERT INTO `Meal` (`Id`, `Name`, `Photo`, `Description`, `QuantityInStock`, `BuyPrice`, `SalePrice`) VALUES
(1, 'Skyscraperburger', 'skyscraper.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum dui vitae lorem commodo, eget euismod erat efficitur. Integer quam metus, laoreet eget vulputate eu.', -1, 9, 18),
(2, 'Blackburger', 'black.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum dui vitae lorem commodo, eget euismod erat efficitur. Integer quam metus, laoreet eget vulputate eu.', 40, 8, 17.5),
(3, 'Superburger', 'burger2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum dui vitae lorem commodo, eget euismod erat efficitur. Integer quam metus, laoreet eget vulputate eu.', -1, 5.85, 14),
(4, 'Vagetarianburger', 'burger3.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum dui vitae lorem commodo, eget euismod erat efficitur. Integer quam metus, laoreet eget vulputate eu.', 19, 4.9, 14.5),
(5, 'Fastburger', 'cheap.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum dui vitae lorem commodo, eget euismod erat efficitur. Integer quam metus, laoreet eget vulputate eu.', -1, 3.4, 9.9),
(6, 'Chickenburger ', 'burger1.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum dui vitae lorem commodo, eget euismod erat efficitur. Integer quam metus, laoreet eget vulputate eu.', -1, 6.7, 14),
(7, 'Shoeburger', 'shoe.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum dui vitae lorem commodo, eget euismod erat efficitur. Integer quam metus, laoreet eget vulputate eu.', -1, 5.6, 20),
(8, 'Cola', 'cola.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum dui vitae lorem commodo, eget euismod erat efficitur. Integer quam metus, laoreet eget vulputate eu.', 127, 1, 3),
(9, 'Coldcoffee', 'coldCoffee.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum dui vitae lorem commodo, eget euismod erat efficitur. Integer quam metus, laoreet eget vulputate eu.', 78, 1.3, 3),
(10, 'Icetea', 'iceTea.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum dui vitae lorem commodo, eget euismod erat efficitur. Integer quam metus, laoreet eget vulputate eu.', 97, 0.8, 3);

-- --------------------------------------------------------

--
-- Table structure for table `Orderline`
--

CREATE TABLE `Orderline` (
  `Id` int(11) NOT NULL,
  `QuantityOrdered` tinyint(4) NOT NULL,
  `Meal_Id` int(11) NOT NULL,
  `Order_Id` int(11) NOT NULL,
  `PriceEach` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Orderline`
--

INSERT INTO `Orderline` (`Id`, `QuantityOrdered`, `Meal_Id`, `Order_Id`, `PriceEach`) VALUES
(1, 1, 1, 1, 18),
(2, 1, 1, 2, 18),
(3, 1, 6, 2, 14),
(4, 1, 5, 2, 9.9),
(5, 1, 7, 2, 20),
(6, 1, 1, 3, 18),
(7, 1, 1, 4, 18),
(8, 1, 1, 5, 18),
(9, 1, 1, 6, 18),
(10, 1, 6, 6, 14),
(11, 1, 5, 6, 9.9),
(12, 1, 7, 6, 20),
(13, 4, 3, 6, 14),
(14, 1, 1, 7, 18),
(15, 1, 6, 7, 14),
(16, 1, 5, 7, 9.9),
(17, 1, 7, 7, 20),
(18, 4, 3, 7, 14),
(19, 1, 3, 8, 14),
(20, 1, 6, 9, 14);

-- --------------------------------------------------------

--
-- Table structure for table `Orders`
--

CREATE TABLE `Orders` (
  `Id` int(11) NOT NULL,
  `User_Id` int(11) NOT NULL,
  `TotalAmount` double NOT NULL,
  `TaxRate` float NOT NULL,
  `TaxAmount` double NOT NULL,
  `CreationTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CompleteTimestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Orders`
--

INSERT INTO `Orders` (`Id`, `User_Id`, `TotalAmount`, `TaxRate`, `TaxAmount`, `CreationTimestamp`, `CompleteTimestamp`) VALUES
(1, 3, 18, 0.21, 3.78, '2016-08-30 10:39:32', '0000-00-00 00:00:00'),
(2, 3, 61.9, 0.21, 12.999, '2016-08-30 10:40:32', '0000-00-00 00:00:00'),
(3, 3, 117.9, 0.21, 24.759, '2016-08-30 10:42:57', '0000-00-00 00:00:00'),
(4, 3, 117.9, 0.21, 24.759, '2016-08-30 10:43:44', '0000-00-00 00:00:00'),
(5, 3, 117.9, 0.21, 24.759, '2016-08-30 10:45:17', '0000-00-00 00:00:00'),
(6, 3, 117.9, 0.21, 24.759, '2016-08-30 11:09:48', '0000-00-00 00:00:00'),
(7, 3, 117.9, 0.21, 24.759, '2016-08-30 11:10:45', '0000-00-00 00:00:00'),
(8, 3, 14, 0.21, 2.94, '2016-08-30 11:11:52', '0000-00-00 00:00:00'),
(9, 3, 14, 0.21, 2.94, '2016-08-30 11:12:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `Id` int(11) NOT NULL,
  `Firstname` varchar(40) NOT NULL,
  `Lastname` varchar(40) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(64) NOT NULL,
  `Birthdate` date DEFAULT NULL,
  `Address` varchar(250) NOT NULL,
  `City` varchar(40) NOT NULL,
  `ZipCode` varchar(5) NOT NULL,
  `Country` varchar(20) DEFAULT NULL,
  `Phone` char(10) NOT NULL,
  `CreationTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastLoginTimestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Role` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`Id`, `Firstname`, `Lastname`, `Email`, `Password`, `Birthdate`, `Address`, `City`, `ZipCode`, `Country`, `Phone`, `CreationTimestamp`, `LastLoginTimestamp`, `Role`) VALUES
(3, 'test', 'test', 'test@test.lt', '098f6bcd4621d373cade4e832627b4f6', '2016-08-05', 'test', 'test', 'test', 'test', 'test', '2016-08-22 16:22:21', '2016-08-30 07:26:40', 0),
(4, 'Ignas', 'Vitkauskas', 'ignas.vit@gmail.com', 'd8578edf8458ce06fbc5bb76a58c5ca4', '2016-08-18', 'Giruliu g. 5-81', 'Vilnius', 'LT-12', 'Lithuania', '62248183', '2016-08-22 16:57:23', '0000-00-00 00:00:00', 0),
(7, 'adminas', 'adminas', 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', '2016-08-03', 'adminas', 'adminas', 'admin', 'adminas', 'adminas', '2016-08-22 17:53:12', '2016-08-28 21:56:57', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Booking`
--
ALTER TABLE `Booking`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `User_Id` (`User_Id`);

--
-- Indexes for table `Meal`
--
ALTER TABLE `Meal`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id` (`Id`);

--
-- Indexes for table `Orderline`
--
ALTER TABLE `Orderline`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Meal_Id` (`Meal_Id`),
  ADD KEY `Order_Id` (`Order_Id`);

--
-- Indexes for table `Orders`
--
ALTER TABLE `Orders`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `User_Id` (`User_Id`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD KEY `Id` (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Booking`
--
ALTER TABLE `Booking`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Meal`
--
ALTER TABLE `Meal`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `Orderline`
--
ALTER TABLE `Orderline`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `Orders`
--
ALTER TABLE `Orders`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Booking`
--
ALTER TABLE `Booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`User_Id`) REFERENCES `Users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Orderline`
--
ALTER TABLE `Orderline`
  ADD CONSTRAINT `orderline_ibfk_1` FOREIGN KEY (`Order_Id`) REFERENCES `Orders` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orderline_ibfk_2` FOREIGN KEY (`Meal_Id`) REFERENCES `Meal` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `Orders`
--
ALTER TABLE `Orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`User_Id`) REFERENCES `Users` (`Id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
